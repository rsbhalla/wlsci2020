% Created 2020-12-02 Wed 10:19
% Intended LaTeX compiler: pdflatex
\documentclass[tiny]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{underscore}
\usepackage[linktocpage,pdfstartview=FitH,colorlinks,linkcolor=blue,anchorcolor=blue,citecolor=DarkGreen,filecolor=blue,menucolor=blue,urlcolor=blue]{hyperref}
\usepackage{attachfile}
\usepackage{minted}
\usepackage[T1]{fontenc}
\usepackage{tgtermes}
\usepackage[scale=.85]{tgheros}
\usepackage{tgcursor}
\usetheme[background=light]{metropolis}
\metroset{block=fill, baground=dark}
\setbeamertemplate{frame footer}{\color{lightgray}\includegraphics[height=0.5cm]{ferallogo.png}}
\usepackage[backend=bibtex]{biblatex}
\BeforeBeginEnvironment{block}{\framebreak}
\institute[INST]{Foundation for Ecological Research, Advocacy and Learning (FERAL). \\\url{https://www.feralindia.org}}
\usepackage{url}
\usepackage{minted}
\usemintedstyle{emacs}
\usetheme{default}
\author{Ravinder Singh Bhalla}
\date{Updated \today}
\title{Using R for Analysing and Visualising Raster Data}
\subtitle{An Introduction}
\begin{document}

\maketitle
\begin{frame}{Outline}
\tableofcontents
\end{frame}


\section{Introduction}
\label{sec:orgcd99d68}

\begin{frame}[label={sec:org06e8045}]{Why use R for Remote Sensing?}
\begin{itemize}
\item Reproducible and reusable code.
\item Multi-threaded.
\item Vast array of analytical tools.
\item Visualisation.
\item Libraries and functions, both from R and from other packages including Python, GRASS, SAGA etc.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgdfd72de},fragile]{What you need}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
libs <- c("sf", "maptools", "raster", "parallel", "ggplot2", "plyr", "reshape2", "scales", "gridExtra", "cowplot", "rcartocolor", "ggspatial", "rgdal", "gdalUtils")
lapply(libs, library, character.only = TRUE)
\end{minted}
\end{frame}

\section{Getting the data in}
\label{sec:orge0a4e42}

\begin{frame}[label={sec:org825177d}]{Types of raster data:}
You can import raster data as:
\begin{itemize}
\item Individual rasters.
\item A stack of rasters is a collection of RasterLayer objects with the same spatial extent and resolution. A RasterStack can be created from RasterLayer objects, or from raster files, or both.
\item A brick of rasters, RasterBrick is a multi-layer raster object. They are typically created from a multi-layer (band) file. They are similar to a RasterStack (that can be created with stack), but are less flexible as they can only point to a single file.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgcc09c60},fragile]{Example of NDVI}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
red <- raster("./data/Mukurthi/b3.tif")
nir <- raster("./data/Mukurthi/b4.tif")
ndvi <- (nir - red) / (nir + red)
image(ndvi)
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/ndvi.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgf6f6187},fragile]{Create a function for NDVI}
 You can create a bunch of functions for indices that you use often. Here is an example of one for ndvi.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
indx.ndvi <- function(red, nir){
ndvi <- (nir - red) / (nir + red)
return(ndvi)
}
\end{minted}
\end{frame}

\begin{frame}[label={sec:orgeed9ba4},fragile]{Using the NDVI function}
 \tiny
Plot function from \url{https://stackoverflow.com/questions/9436947/legend-properties-when-legend-only-t-raster-package}
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
library(colorRamps )
r <- indx.ndvi(red = red, nir = nir)
plot(r, col = blue2green(100), legend=FALSE, axes=FALSE)
r.range <- c(round(minValue(r), 2), round(maxValue(r), 2))
plot(r, legend.only=TRUE, col=blue2green(100 ),
     legend.width=1, legend.shrink=0.75,
     axis.args=list(at=seq(r.range[1], r.range[2], 25),
                    labels=seq(r.range[1], r.range[2], 25),
                    cex.axis=0.6),
     legend.args=list(text='NDVI', side=4, font=2, line=2.5, cex=0.8))
\end{minted}
\begin{center}
\includegraphics[width=0.25\textwidth]{./res/func-ndvi.png}
\end{center}
\end{frame}

\section{A sample of my work in South Sudan}
\label{sec:org556ee2b}
\begin{frame}[label={sec:org3f286e0}]{A note on this data}
\begin{itemize}
\item This is from my present work in South Sudan where I'm in the final stages of describing biophysical attributes of sites.
\item Government colleagues are still finalising the actual micro-watersheds to select.
\item I need to be able to complete this analysis as soon as they pick three of these micro-watersheds.
\item Vector data used here is from public sites including the National Bureau of Statistics (RoSS), World Bank and OCHA.
\item Climatic trends were extracted from USGS data on Google Earth Engine with many inputs from Pradeep Koulgi.
\item The LULC data is from ESA and is both the rasters and a csv file that holds the classes.
\item Other data sources have been shared with you earlier.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org0fce499}]{The Republic of South Sudan with Waterbodies and PAs}
\begin{center}
\includegraphics[width=.9\linewidth]{./RoSS.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgbbdcf39}]{Map of the potential sites (micro-watersheds)}
\begin{center}
\includegraphics[height=1.0\textheight]{./MicWS.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgf977275},fragile]{Import the Rasters}
 These have been clipped to the extent of the two counties which were pre-selected for the project.
Note: this is a subset of the data.
Also note: Lists are more efficient, the last line converts the list into its respective parts and can be dropped if you're comfortable with lists.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
rst.list <- list.files("./data/clim", full.names=TRUE, pattern=".tif$")
rst.names <- gsub(".tif", "", list.files("./data/clim", full.names=FALSE, pattern = ".tif$"))
lst <- lapply(rst.list, FUN = raster)
names(lst) <- rst.names
list2env(lst, envir=.GlobalEnv) # extract the elements of the list
\end{minted}
\end{frame}

\begin{frame}[label={sec:org9488952},fragile]{Import the Vector}
 I've got three probable micro-watersheds, but I can always change these later if asked to.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
selmws <- st_read(dsn = "./data/clim/RoSS.gpkg", layer = "SelWS")
plot(st_geometry(selmws), col = "red")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./SelectedMicWS.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orga266610},fragile]{Define the objective}
 I need to figure out what the trends and the land use under my selected micro-watersheds is. I need the results in the form of a bar plot which is simple to understand by non-tech folks.
Therefore my script should be able to:
\begin{enumerate}
\item Clip the raster to the extent of the selected micro-watersheds.
\item Extract the values of the rasters as a histogram in the case of continuous data (trends) OR as categories for the LULC and similar categorical maps.
\item Visualise the results dump them as graphics I can import into my reports.
\end{enumerate}

To do this I created two functions which I could \texttt{apply} onto my data. This is similar to what Pradeep did with his functions which he \texttt{mapped} the data to in the Earth Engine.
\end{frame}

\begin{frame}[label={sec:orgd4f5db3},fragile]{Fn.1: Histogram of continuous raster within vector boundaries}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos,fontsize=\ttfamily\tiny]{r}
hist.continuous <- function(rst, vect, filename) {
  # transform vect to the rasters crs
  crs.to <- crs(rst, asText = TRUE)
  if(crs.to != crs(vect, asText = TRUE)) {
    vect <- st_transform(vect, crs.to )
  }
  rst <- crop(rst, extent(vect))
  rst <- mask(rst, vect)
  if(crs(rst, asText = TRUE) == "+proj=longlat +datum=WGS84 +no_defs") {
    rst <- projectRaster(rst, crs = 20135 )
  } # transform raster to utm
  f <- hist(rst, breaks=30, plot = FALSE)
  dat <- data.frame(counts= f$counts, breaks = f$mids)
  dat$area_ha <- prod(res(rst))*dat$counts/10000 # multiply frequency with pixel resolution
  colnames(dat ) <- c("Pixels", "Value",  "AreaHa")
  plt <- ggplot(dat, aes(x = Value, y = AreaHa, fill = Value)) +
    geom_bar(stat = "identity", alpha = 0.8)+
    xlab("xlab")+
    ylab("Area Ha")+
    guides(fill=guide_legend(title="xlab"))+
    scale_x_continuous() +
    scale_fill_gradient(low="red", high="green")
## plt
  ggsave(plot = plt,  filename = filename, device = "png", width = 12, height = 8, units = "in", dpi = 300)
 }
\end{minted}
\end{frame}

\begin{frame}[label={sec:orgac02c7d},fragile]{Lets run it on the Palmer Drought Severity Index}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
hist.continuous(rst = pdsiTrendsAweil_Max.Annual, vect = selmws, filename = "./res/pdsiHist.png")
\end{minted}
\begin{center}
\includegraphics[width=0.65\textwidth]{./res/pdsiHist.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org186648c},fragile]{Fn. 2: Histogram of categorical raster within vector boundaries}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos,fontsize=\ttfamily\tiny]{r}
hist.categorised <- function(rst, vect, class, filename) {
  # transform vect to the rasters crs
  crs.to <- crs(rst, asText = TRUE)
  if(crs.to != crs(vect, asText = TRUE)) {
    vect <- st_transform(vect, crs.to )
  }
  rst <- crop(rst, extent(vect))
  rst <- mask(rst, vect)
  if(crs(rst, asText = TRUE) == "+proj=longlat +datum=WGS84 +no_defs") {
    rst <- projectRaster(rst, crs = 20135 )
  } # transform raster to utm
  rst.df <- as.data.frame(rst)
  names(rst.df) <- "Category"
  colnames(vegclass)[c(1, 2)] <- c("Category", "Cover")
  rst.df <- merge(rst.df, vegclass, by = "Category")
  rst.df <- count(rst.df, "Cover" )
  rst.df$AreaHa <- prod(res(rst))*rst.df$freq / 10000 # multiply frequency with pixel resolution
  plt <- ggplot(rst.df, aes(x=Cover, y=AreaHa, fill = Cover)) +
    geom_bar(stat = "identity") +
    scale_fill_brewer(palette = "Set1") +
    labs(y = "Area in Ha")+
    scale_x_discrete(guide = guide_axis(n.dodge = 2))+
    theme(
      legend.position="none",
      axis.text.x = element_text( vjust = 0.5, hjust= 0.5))
  # plt
  ggsave(plot = plt,  filename = filename, device = "png", width = 12, height = 8, units = "in", dpi = 300)
}
\end{minted}
\end{frame}

\begin{frame}[label={sec:org9ee7691},fragile]{Run in on the European Space Agency LULC map of Africa}
 Read up about the dataset \href{http://2016africalandcover20m.esrin.esa.int/}{here}.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
lulc.class <- read.csv("./data/clim/lulc.csv", sep=";")
hist.categorised(rst = lulc, vect = selmws, class = lulc.class, filename = "./res/lulcHist.png")
\end{minted}
\begin{center}
\includegraphics[width=0.65\textwidth]{./res/lulcHist.png}
\end{center}
\end{frame}

\section{Optional Exercises}
\label{sec:orgb1496e7}
\begin{frame}[label={sec:orgb036cdd}]{Consider This}
\begin{itemize}
\item A list of functions for the indices you use most often (perhaps along with plotting).
\item A ggplot2 function to such as the ones above to help describe your raster data.
\end{itemize}
\end{frame}

\section{Resources}
\label{sec:orgce4aead}

\begin{frame}[label={sec:org65db084}]{Raster analysis}
\begin{itemize}
\item \href{https://cran.r-project.org/web/packages/raster/raster.pdf}{Raster package documentation}.
\end{itemize}
\end{frame}
\end{document}
