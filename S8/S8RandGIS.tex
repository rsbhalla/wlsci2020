% Created 2020-11-30 Mon 19:31
% Intended LaTeX compiler: pdflatex
\documentclass[tiny]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{underscore}
\usepackage[linktocpage,pdfstartview=FitH,colorlinks,linkcolor=blue,anchorcolor=blue,citecolor=DarkGreen,filecolor=blue,menucolor=blue,urlcolor=blue]{hyperref}
\usepackage{attachfile}
\usepackage{minted}
\usepackage[T1]{fontenc}
\usepackage{tgtermes}
\usepackage[scale=.85]{tgheros}
\usepackage{tgcursor}
\usetheme[background=light]{metropolis}
\metroset{block=fill, baground=dark}
\setbeamertemplate{frame footer}{\color{lightgray}\includegraphics[height=0.5cm]{ferallogo.png}}
\usepackage[backend=bibtex]{biblatex}
\BeforeBeginEnvironment{block}{\framebreak}
\institute[INST]{Foundation for Ecological Research, Advocacy and Learning (FERAL). \\\url{https://www.feralindia.org}}
\usepackage{minted}
\usemintedstyle{emacs}
\usetheme{default}
\author{Ravinder Singh Bhalla}
\date{Updated \today}
\title{Using R for GIS}
\subtitle{An Introduction}
\begin{document}

\maketitle
\begin{frame}{Outline}
\tableofcontents
\end{frame}

\section{Introduction}
\label{sec:org580ed56}

\begin{frame}[label={sec:org911bc00}]{Summary}
A quick and dirty demo of using spatial analysis packages in R. This includes:
\begin{itemize}
\item An introduction to the simple features: \url{https://cran.r-project.org/web/packages/sf/index.html}
\item An introduction to the raster package: \url{https://cran.r-project.org/web/packages/raster/index.html}
\item An introduction to ggmap and assorted tools for map building in R.
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgfadbb19}]{Why should you use R for spatial analysis?}
\begin{enumerate}
\item Efficient, multi-core.
\item Reusable code.
\item Wide(r) set of tools and libraries than many GUI packages.
\item Seamless integration with statistical analysis.
\item Fantastic visualisation and plotting.
\end{enumerate}
\end{frame}

\begin{frame}[label={sec:org94d2071}]{Downsides of using R}
\begin{itemize}
\item You need to 'prepare' the data using standard GIS packages. I use:
\begin{itemize}
\item QGIS or GRASS for digitising.
\item GRASS for hydrology work such as catchment delineations.
\item Google Earth Engine for trend analysis and large datasets.
\end{itemize}
\item Visual cues are missing - errors not always obvious and need to be 'seen'.
\item Number of packages can be overwhelming.
\item Fancy layout not really an option (yet).
\end{itemize}
\end{frame}

\section{Getting Started with Simple Features}
\label{sec:org92cdc3b}

\begin{frame}[label={sec:orgddfcbaa}]{\href{https://r-spatial.github.io/sf/index.html}{From the sf site}}
Simple Features is a package that provides simple features access for R. Package sf:
\footnotesize
\begin{itemize}
\item represents simple features as records in a data.frame or tibble with a geometry list-column
\item represents natively in R all 17 simple feature types for all dimensions (XY, XYZ, XYM, XYZM)
\item interfaces to \href{https://trac.osgeo.org/geos}{GEOS} to support geometrical operations including the \href{https://en.wikipedia.org/wiki/DE-9IM}{DE9-IM}
\item interfaces to \href{http://www.gdal.org/}{GDAL}, supporting all driver options, Date and POSIXct and list-columns
\item interfaces to \href{http://proj.org/}{PRØJ} for coordinate reference system conversions and transformations
\item uses \href{https://en.wikipedia.org/wiki/Well-known\_text\#Well-known\_binary}{well-known-binary} serialisations written in C++/Rcpp for fast I/O with GDAL and GEOS
\item reads from and writes to spatial databases such as \href{http://postgis.net/}{PostGIS} using \href{https://cran.r-project.org/web/packages/DBI/index.html}{DBI}
\item is extended by pkg \href{https://github.com/r-spatial/lwgeom/}{lwgeom} for further liblwgeom/PostGIS functions, including some spherical geometry functions
\end{itemize}
\end{frame}


\begin{frame}[label={sec:org7dbd01c},fragile]{Set the working environment and directories}
 I like to explicitly state my working directory and set my input directory to \texttt{data} and results to \texttt{res}.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
wd <- "./"
resdir <- paste0(getwd(), "/res/")
datadir <- paste0(getwd(), "/data/")
dsn <- paste0(datadir, "/Kalivelli/SFplotsKalivelliLL.gpkg")
setwd(wd)
getwd()
\end{minted}

\begin{verbatim}
/home/udumbu/rsb2/NCBS/wlsci2020/S8
\end{verbatim}
\end{frame}

\begin{frame}[label={sec:orgc4ed629},fragile]{Libraries}
 Some of these packages may be redundant:
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
libs <- c("raster", "data.table", "ggplot2", "reshape2", "parallel","gdalUtils", "sf", "lwgeom", "ggthemes", "rasterVis", "viridis", "ggforce", "scales", "latex2exp", "gridExtra")
lapply(libs, library, character.only = TRUE)
\end{minted}
\end{frame}

\begin{frame}[label={sec:org4ac05f9},fragile]{Setting options}
 Its a good idea to set some environmental variables
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
tiffoptions <- c("COMPRESS=DEFLATE", "PREDICTOR=2", "ZLEVEL=9") # options to write compressed tiffs
options(width = 120) # I use emacs-ESS and prefer more text per line.
# set up cairo for rendering <https://www.r-bloggers.com/2012/10/using-consistent-r-and-latex-fonts-in-org-or-knitr-or-sweave/>
  library(Cairo)
  mainfont <- "tgtermes"
  CairoFonts(regular = paste(mainfont,"style=Regular",sep=":"),
             bold = paste(mainfont,"style=Bold",sep=":"),
             italic = paste(mainfont,"style=Italic",sep=":"),
             bolditalic = paste(mainfont,"style=Bold Italic,BoldItalic",sep=":"))
  pdf <- CairoPDF
  png <- CairoPNG
\end{minted}
\end{frame}

\begin{frame}[label={sec:org8af0a6f},fragile]{Call your routines}
 I have a collection of functions that I've put together over the years. Please use with care - some of them call multiple cores and \alert{WILL} crash your machine if you don't tweak them.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
source("fun.GeoProc.R", echo = FALSE) # GIS functions with plotting
source("fun.Trends.R", echo = FALSE) # Raster functions with plotting
\end{minted}
\end{frame}

\section{Some basic vector work}
\label{sec:orga411751}

\begin{frame}[label={sec:org48f7724},fragile]{Importing vector data}
 First we bring in the raw data from a shapefile and a geopackage
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
ResMap <- st_read(dsn = paste0(datadir, "/Kalivelli"),  layer = "ResMap06Mar2020")
KalivelliBound <- st_read(dsn = dsn,  layer = "KalivelliBound" )
VillageBound <- st_read(dsn = dsn,  layer = "VillageBound" )
\end{minted}
\end{frame}

\begin{frame}[label={sec:orgf910a32},fragile]{Exploring a simple features object}
 It looks like a data frame however has a geometry column.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
ResMap
\end{minted}
\tiny
\begin{verbatim}
Simple feature collection with 96 features and 9 fields
geometry type:  POLYGON
dimension:      XY
bbox:           xmin: 79.79801 ymin: 12.0681 xmax: 79.91085 ymax: 12.16698
geographic CRS: WGS 84
First 10 features:
   id         Resource      Village Caste    Ownership Numbers        Descri            Status      Unit
1  NA      Shrimp farm        Urani    SC    Marakanam       6 Kaliveli land            Before Household
2  NA     Grazing area        Urani    SC        Urani     100 Kaliveli land            Before       Cow
3  NA     Grazing area        Urani    SC        Urani     200 Kaliveli land            Before      Goat
4  NA     Fishing area        Urani    SC        Urani      20 Kaliveli land            Before Household
5  NA Agriculture land        Urani    SC        Urani      20 Kaliveli land After Shrimp farm Household
6  NA     Grazing area        Urani    SC        Urani     100 Kaliveli land             After       Cow
7  NA     Grazing area        Urani    SC        Urani     200 Kaliveli land             After      Goat
8  NA Agriculture land Chittikuppam    SC Chittikuppam      15 Kaliveli land            Before Household
9  NA     Grazing area Chittikuppam    SC Chittikuppam     100 Kaliveli land            Before       Cow
10 NA     Grazing area Chittikuppam    SC Chittikuppam     200 Kaliveli land            Before      Goat
                         geometry
1  POLYGON ((79.89726 12.15441...
2  POLYGON ((79.89726 12.15441...
3  POLYGON ((79.89726 12.15441...
4  POLYGON ((79.89726 12.15441...
5  POLYGON ((79.89978 12.15665...
6  POLYGON ((79.89722 12.15375...
7  POLYGON ((79.89722 12.15375...
8  POLYGON ((79.8892 12.12564,...
9  POLYGON ((79.88769 12.11854...
10 POLYGON ((79.88769 12.11854...
\end{verbatim}
\end{frame}


\begin{frame}[label={sec:org090d58a},fragile]{It also contains projection information}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
st_crs(ResMap)
\end{minted}
\end{frame}

\begin{frame}[label={sec:org3319539},fragile]{You can plot the geometry}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
plot(st_geometry(ResMap))
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/ResMap.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org1bc1211},fragile]{Start adding other maps}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
plot(st_geometry(VillageBound), col="grey95")
plot(st_geometry(KalivelliBound), col="pink", add=TRUE)
plot(st_geometry(ResMap), col="transparent", border="brown", add=TRUE)
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/ResMap2.png}
\end{center}
\end{frame}

\section{Generating maps with R}
\label{sec:org9377704}

\begin{frame}[label={sec:org633fb9f},fragile]{First generate a bunch of plots}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos,fontsize=\ttfamily\tiny]{r}
plt <- lapply(unique(ResMap$Resource),  FUN = function(x) {
  resmp <- ResMap[ResMap$Resource == x,]
  before <- resmp[resmp$Status == "Before",]
  after <- resmp[resmp$Status == "After",]
  subplt <- ggplot() +
    geom_sf(data = VillageBound, aes(), color = "grey", size = 0.1) +
    geom_sf(data = KalivelliBound, aes(), color = "lightgreen", fill = "transparent", size = 0.3) +
    geom_sf(data = before, aes(),color = "blue", linetype = "12", size = 0.5, fill = "transparent") +
    geom_sf(data = after, aes(),color = "red", linetype = "12", size = 0.5, fill = "transparent") +
    theme(plot.title = element_text(size = rel(0.75), face = "bold"),
      axis.title=element_text(size=rel(0.75)),
      axis.text=element_text(size=rel(0.55)),
      axis.text.y=element_text(angle=90, hjust=0.5, size=rel(0.75)),
      axis.text.x=element_text(vjust=0.5, size=rel(0.55)),
      legend.text = element_text(size=rel(0.75)),
      legend.title = element_text(size=rel(0.75), face = "bold"),
      legend.position="right",
      panel.grid.major = element_line(color = "grey",  linetype = 2, size=0.1),
      plot.margin = unit(c(.02,.02,.02,.02), "cm")) +
    labs(x = "Longitude", y = "Latitude", title = x)
  return(subplt)
})
\end{minted}
\end{frame}

\begin{frame}[label={sec:orgc45648d},fragile]{Then use grid.arrange to arrange them on panels}
 Here is the function
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos,fontsize=\ttfamily\tiny]{r}
plt.panel <- function(plt, rws, cls, flnm, top.txt, wd, ht){
    no.plts <- rws * cls
    n.panels <- length(plt)/no.plts
    if(trunc(n.panels) < n.panels) n.panels <- trunc(n.panels + 1)
    lapply(1:n.panels, function(n){
        panel.name <- paste0("Panel", n)
        panel.plt <- plt[((n-1)*no.plts+1):(n*no.plts)]
        panel.plt[sapply(panel.plt, is.null)] <- NULL
        margin = theme(plot.margin = unit(c(.02,.02,.02,.02), "cm"))
        plts <- do.call(grid.arrange,
                        c(panel.plt,
                          top = top.txt,
                          nrow = rws, ncol = cls))
        flnm <- gsub(".png", paste0(panel.name, ".png"), flnm)
        ggsave(plot = plts, filename = flnm, dpi = 450,
               width = wd, height = ht, units = "in")
    })
}
\end{minted}
\end{frame}

\begin{frame}[label={sec:orgf26ef5e},fragile]{Call the function and visualise the map}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
plt.panel(plt = plt, rws = 2, cls = 5, flnm = "./res/test.png", top.txt = "Resource Use in Kalivelli", wd = 10, ht = 5)
\end{minted}

\begin{center}
\includegraphics[width=1\textwidth]{./res/testPanel1.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org1937517}]{Your task}
\begin{block}{Before we go to the next section}
Deconstruct the code on slide 15 to generate a plot of a single resource (not the panel of 10 maps).
\end{block}
\end{frame}

\section{Vector geometry operations with R}
\label{sec:org4fc598a}

\begin{frame}[label={sec:orga5611ce}]{Asking spatial questions}
\begin{itemize}
\item SF provides a number of very convenient functions - very similar but not as exhaustive as PostGIS.
\item Let us, in the next bunch of exercises, use the simple features to answer some spatial questions.
\item We'll need to call a bunch of functions for this
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org0077346},fragile]{What are the functions that sf offers?}
 \url{https://r-spatial.github.io/sf/reference/index.html}

\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
methods(class = "sf")
\end{minted}
\tiny
\begin{verbatim}
 [1] [                          [[<-                       $<-                        aggregate
 [5] as.data.frame              cbind                      coerce                     dbDataType
 [9] dbWriteTable               extent                     extract                    filter
[13] identify                   initialize                 mask                       merge
[17] plot                       print                      raster                     rasterize
[21] rbind                      select                     show                       slotsFromS3
[25] st_agr                     st_agr<-                   st_area                    st_as_sf
[29] st_bbox                    st_boundary                st_buffer                  st_cast
[33] st_centroid                st_collection_extract      st_convex_hull             st_coordinates
[37] st_crop                    st_crs                     st_crs<-                   st_difference
[41] st_filter                  st_force_polygon_cw        st_geometry                st_geometry<-
[45] st_interpolate_aw          st_intersection            st_intersects              st_is_polygon_cw
[49] st_is_valid                st_is                      st_join                    st_line_merge
[53] st_linesubstring           st_m_range                 st_make_valid              st_minimum_bounding_circle
[57] st_nearest_points          st_node                    st_normalize               st_point_on_surface
[61] st_polygonize              st_precision               st_reverse                 st_sample
[65] st_segmentize              st_set_precision           st_shift_longitude         st_simplify
[69] st_snap_to_grid            st_snap                    st_split                   st_subdivide
[73] st_sym_difference          st_transform_proj          st_transform               st_triangulate
[77] st_union                   st_voronoi                 st_wrap_dateline           st_write
[81] st_z_range                 st_zm                      transform
see '?methods' for accessing help and source code
\end{verbatim}
\end{frame}

\begin{frame}[label={sec:orgcf4647f},fragile]{Get a look at the data}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
names(ResMap)
\end{minted}

\begin{center}
\begin{tabular}{l}
id\\
Resource\\
Village\\
Caste\\
Ownership\\
Numbers\\
Descri\\
Status\\
Unit\\
geometry\\
\end{tabular}
\end{center}
\end{frame}

\begin{frame}[label={sec:org608cd6d},fragile]{Get the area of features}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
ResMap$area_ha <- st_area(ResMap)/10000
ResMap.df <- ResMap
st_geometry(ResMap.df) <- NULL
head(ResMap.df)[,c("Village", "Resource", "area_ha")]
\end{minted}

\begin{center}
\begin{tabular}{llr}
Urani & Shrimp farm & 43.0165879033885\\
Urani & Grazing area & 25.5193479961216\\
Urani & Grazing area & 25.5193479961216\\
Urani & Fishing area & 5.77894998383988\\
Urani & Agriculture land & 7.34096818723707\\
Urani & Grazing area & 69.0820504990389\\
\end{tabular}
\end{center}
\end{frame}

\begin{frame}[label={sec:org6dea283},fragile]{Start aggregating}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
agg <- aggregate(list(ResMap$Numbers, ResMap$area_ha), by = list(ResMap$Village, ResMap$Resource), sum)
colnames(agg) <- c("Village", "Resource","Numbers", "AreaHa")
print(agg)
\end{minted}
\tiny
\begin{verbatim}
            Village          Resource Numbers            AreaHa
1          Aruvadai  Agriculture land      60   67.990115 [m^2]
2      Chittikuppam  Agriculture land      45    6.789057 [m^2]
3        Devenandal  Agriculture land      50   43.766248 [m^2]
4  Kazhuperumpakkam  Agriculture land     334  337.510382 [m^2]
5        Kilapakkam  Agriculture land     100  161.104110 [m^2]
6             Urani  Agriculture land      20    7.340968 [m^2]
7      Chittikuppam           FD pond       1   26.115491 [m^2]
8      Chittikuppam      Fishing area     150   83.897191 [m^2]
9        Kilapakkam      Fishing area      60   67.265447 [m^2]
10     Seyyankuppam      Fishing area      30   13.811527 [m^2]
11            Urani      Fishing area      20    5.778950 [m^2]
12         Aruvadai      Grazing area    4450  948.091746 [m^2]
13     Chittikuppam      Grazing area    1200  327.661254 [m^2]
14       Devenandal      Grazing area    1100   26.267849 [m^2]
15       Edaicherry      Grazing area    3500  497.844736 [m^2]
16 Kazhuperumpakkam      Grazing area    2700 1070.372800 [m^2]
17       Kilapakkam      Grazing area    2000 1185.888199 [m^2]
18         Koluvari      Grazing area    5000 1315.600996 [m^2]
19            Urani      Grazing area     600  189.202797 [m^2]
20      Vilvanatham      Grazing area    3800  260.899877 [m^2]
21         Aruvadai Mixed Agriculture      75   53.337511 [m^2]
22       Devanandal Mixed Agriculture      20   23.006486 [m^2]
23       Edaicherry Mixed Agriculture     300  248.922368 [m^2]
24      Vilvanatham Mixed Agriculture     300  872.775231 [m^2]
25         Aruvadai          Prosopis      50   18.323564 [m^2]
26     Chittikuppam          Prosopis      11  109.321585 [m^2]
27 Kazhuperumpakkam          Prosopis     502  159.557787 [m^2]
28       Kilapakkam          Prosopis     200  148.650354 [m^2]
29         Aruvadai             Reeds     400  158.343309 [m^2]
30         Koluvari             Reeds       1  231.565209 [m^2]
31     Chittikuppam             Sambu      75  149.362707 [m^2]
32       Kilapakkam             Sambu     100   67.265447 [m^2]
33     Seyyankuppam             Sambu       4   12.850129 [m^2]
34            Urani       Shrimp farm       6   43.016588 [m^2]
35     Chittikuppam            Vandal      50   58.766023 [m^2]
36       Kilapakkam            Vandal     100  215.915801 [m^2]
\end{verbatim}
\end{frame}

\begin{frame}[label={sec:orgbfb93b1}]{Use different formulae and different attributes to explore the data}
\begin{example}[For Example]
\begin{itemize}
\item change the function "FUN = "
\item Filter the data to relevant fields and conditions.
\end{itemize}
\end{example}
\end{frame}

\begin{frame}[label={sec:org3f81ae6},fragile]{Start asking questions}
 \begin{example}[Which caste group lost out more on access to grazing lands]
\begin{itemize}
\item Filter the data for before and after declaration of the sanctuary (Status) and the grazing areas (Resource)
\end{itemize}
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
grazing.before <- ResMap[with(ResMap, Resource == "Grazing area" & Status == "Before"),]
grazing.after  <- ResMap[with(ResMap, Resource == "Grazing area" & Status == "After"),]
\end{minted}
\end{example}
\end{frame}

\begin{frame}[label={sec:orgf5c9eed},fragile]{You can visualise this}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
plot(st_geometry(KalivelliBound), col="blue")
plot(st_geometry(grazing.before), col="red", add=TRUE)
plot(st_geometry(grazing.after),  col="transparent", border="green", add=TRUE)
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/CattleBeforeAfter.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgd69d8d9},fragile]{You can/should also do some plots}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
dat <- ResMap[with(ResMap, Resource == "Grazing area"),]
ggplot(dat, aes(x=Status, y=as.numeric(area_ha))) +
  geom_boxplot() +
  facet_wrap(. ~ Village, scales = 'free') +
  labs(title="Area Under Grazing",x="Before/After", y = "Area in Ha")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/GrazingAreaVillages.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgbd7622c},fragile]{And some stats}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
dat <- ResMap[with(ResMap, Resource == "Grazing area"),]
t.test(as.numeric(area_ha)~Status, data=dat)
\end{minted}
\tiny
\begin{verbatim}

	Welch Two Sample t-test

data:  as.numeric(area_ha) by Status
t = 2.4784, df = 29.112, p-value = 0.01924
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
  17.43128 181.88283
sample estimates:
 mean in group After mean in group Before
           177.61299             77.95593
\end{verbatim}
\end{frame}

\begin{frame}[label={sec:org94eb388}]{Is resource use significantly different:}
\begin{itemize}
\item Before/after sanctuary
\item Before/after sanctuary between villages
\item Before/after sanctuary between castes
\item Before/after sanctuary between villages and castes
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org64635d6}]{Exercise: Try and create functions from the procedures we just carried out}
\end{frame}

\section{Filtering and merging simple features}
\label{sec:org9b349ba}
\begin{frame}[label={sec:org6f02ad4},fragile]{Three basic functions (there are more)}
 You've already seen an example of \href{https://rdrr.io/cran/sf/man/aggregate.sf.html}{\texttt{aggregate}} on simple features objects. The \href{https://rdrr.io/cran/sf/man/tidyverse.html}{\texttt{tidyverse}} environment is often used as well. Here we'll go through three types of functions:
\begin{itemize}
\item Square brackets work as expected on simple features.
\item The merge function works well on simple features.
\item The \href{https://rdrr.io/cran/sf/man/st\_join.html}{st\textsubscript{join}} function is a powerful wrapper to extract and merge data from spatial objects.
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org0db7496},fragile]{The data}
 We'll use a pair of point layers, line layers and polygon layers for these exercises. Download the data from \href{data/Mukurthi/S8sfjoin.gpkg }{here} and add it to your R session with these commands:

\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
dsn <- "./data/Mukurthi/S8sfjoin.gpkg"
names(lyrs )
lyrs <- st_layers(dsn) # get names of layers
lst <- lapply(lyrs$name, FUN = function(x) {
  lst <- st_read(dsn = dsn, layer = x)
  return(lst )
})
names(lst ) <- lyrs$name # name the elements of the list
list2env(lst, envir=.GlobalEnv) # extract the elements of the list
\end{minted}

Go ahead and visualise them if you like using \texttt{plot(OBJECT)}, where object is the name of the simple features object. You may also use the notation \texttt{plot(st\_geometry(OBJECT))}. I prefer using the latter.
\end{frame}

\begin{frame}[label={sec:org0ae5588},fragile]{Square brackets}
 Indexing works on sf objects so you can subset them using variants of the example below.
Working with the sf object \texttt{physiognomy}
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
physiognomy$area_ha <- as.numeric(st_area(physiognomy)) / 10000 # get area in ha
physiognomy <- physiognomy[physiognomy$area_ha > 5000, ] # area greater than 5000 ha
\end{minted}
\end{frame}

\begin{frame}[label={sec:org9b8b346},fragile]{Merging}
 Lets slap the physiognomy codes onto the vector

\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
physiognomy <- merge(x = physiognomy, y = Physiognomy_codesfixed, by.x ="physio", by.y = "phcode2")
library("smoothr")
pol <- fill_holes(physiognomy, threshold = 2000000)
plot(st_geometry(pol), col = "red")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/Pol.png}
\end{center}
\end{frame}

\section{Joining attributes using spatial relationships}
\label{sec:org46bba20}
\begin{frame}[label={sec:org80f7563}]{Use this figure as a reference}
\begin{itemize}
\item pl1 is blue
\item pl2 is yellow
\end{itemize}
\begin{center}
\includegraphics[width=0.55\textwidth]{./Pl1Pl2.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orga7c06f9},fragile]{A join without a predicate assumes \texttt{st\_intersects}}
 You can use a \href{https://www.rdocumentation.org/packages/sf/versions/0.9-6/topics/st\_join}{bunch of conditions} or filters to do a join between spatial objects. However, an inner join and a left join give completely different results.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12r <- st_join(pl1, pl2, join = st_intersects, left=FALSE) # same as  st_join(veg_mt, roads,  left=FALSE)
plot(st_geometry(pl12r), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinR.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org105d93d},fragile]{Add the option \texttt{left=TRUE}}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12l <- st_join(pl1, pl2, join=st_intersects, left=TRUE) # this is the default
plot(st_geometry(pl12l), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinL.png}
\end{center}
\end{frame}


\begin{frame}[label={sec:org93c8e3f},fragile]{With \texttt{st\_touches} and \texttt{left=FALSE}}
 Check out the two layers, there's only one polygon on pl1 which touches (shares the same border) with another in pl2.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12r <- st_join(pl1, pl2, join = st_touches, left=FALSE) # same as  st_join(veg_mt, roads,  left=FALSE)
plot(st_geometry(pl12r), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinRtouches.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org21bb027},fragile]{\texttt{st\_touches} and \texttt{left=TRUE}}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12l <- st_join(pl1, pl2, join=st_touches, left=TRUE) # this is the default
plot(st_geometry(pl12l), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinLtouches.png}
\end{center}
\end{frame}


\begin{frame}[label={sec:org9c541cb},fragile]{With \texttt{st\_overlaps} and \texttt{left=FALSE}}
 Overlaps excludes the polygons that are touching or those falling inside.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12r <- st_join(pl1, pl2, join = st_overlaps, left=FALSE) # same as  st_join(veg_mt, roads,  left=FALSE)
plot(st_geometry(pl12r), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinRoverlaps.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgc32c348},fragile]{\texttt{st\_overlaps} and \texttt{left=TRUE}}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12l <- st_join(pl1, pl2, join=st_overlaps, left=TRUE) # this is the default
plot(st_geometry(pl12l), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinLoverlaps.png}
\end{center}
\end{frame}


\begin{frame}[label={sec:org289166c},fragile]{With \texttt{st\_contains} and \texttt{left=FALSE}}
 Contains implies the polygon in pl1 has to have a smaller polygon from pl2 completely within its boundary.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12r <- st_join(pl1, pl2, join = st_contains, left=FALSE) # same as  st_join(veg_mt, roads,  left=FALSE)
plot(st_geometry(pl12r), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinRcontains.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org8e7d509},fragile]{\texttt{st\_contains} and \texttt{left=TRUE}}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12l <- st_join(pl1, pl2, join=st_contains, left=TRUE) # this is the default
plot(st_geometry(pl12l), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinLcontains.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgaff8732},fragile]{With \texttt{st\_covers} and \texttt{left=FALSE}}
 You can use a \href{https://www.rdocumentation.org/packages/sf/versions/0.9-6/topics/st\_join}{bunch of conditions} or filters to do a join between spatial objects. However, an inner join and a left join give completely different results.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12r <- st_join(pl1, pl2, join = st_covers, left=FALSE) # same as  st_join(veg_mt, roads,  left=FALSE)
plot(st_geometry(pl12r), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinRcovers.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org25b439d},fragile]{\texttt{st\_covers} and \texttt{left=TRUE}}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12l <- st_join(pl1, pl2, join=st_covers, left=TRUE) # this is the default
plot(st_geometry(pl12l), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinLcovers.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org0cfd32e},fragile]{With \texttt{st\_covered\_by} and \texttt{left=FALSE}}
 You can use a \href{https://www.rdocumentation.org/packages/sf/versions/0.9-6/topics/st\_join}{bunch of conditions} or filters to do a join between spatial objects. However, an inner join and a left join give completely different results.
\begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12r <- st_join(pl1, pl2, join = st_covered_by, left=FALSE) # same as  st_join(veg_mt, roads,  left=FALSE)
plot(st_geometry(pl12r), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinRcoveredby.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org43d07fa},fragile]{\texttt{st\_covered\_by} and \texttt{left=TRUE}}
 \begin{minted}[frame=lines,fontsize=\scriptsize,breaklines,linenos]{r}
pl12l <- st_join(pl1, pl2, join=st_covered_by, left=TRUE) # this is the default
plot(st_geometry(pl12l), col="lightslateblue")
\end{minted}
\begin{center}
\includegraphics[width=0.45\textwidth]{./res/stjoinLcoveredby.png}
\end{center}
\end{frame}

\section{Resources}
\label{sec:org1b80597}

\begin{frame}[label={sec:org10af4f6}]{Books}
\begin{itemize}
\item \href{https://geocompr.robinlovelace.net/}{Geocomputation with R}: An open source book which covers all of the above with fantastic examples and exercises.
\end{itemize}
\end{frame}
\end{document}
