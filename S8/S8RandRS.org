#+LANGUAGE: en
#+TITLE: Using R for Analysing and Visualising Raster Data
#+SUBTITLE: An Introduction
#+DATE: Updated \today
#+email: bhalla@feralindia.org
#+LANGUAGE:  en
#+OPTIONS:   H:2 num:t toc:t \n:nil ::t |:t ^:t -:t f:t *:t tex:t d:(HIDE) tags:not-in-toc <:t
#+OPTIONS:   d:nil todo:t pri:nil email:nil

#+LaTeX_HEADER: \usepackage[T1]{fontenc}
#+LaTeX_HEADER: \usepackage{tgtermes}
#+LaTeX_HEADER: \usepackage[scale=.85]{tgheros}
#+LaTeX_HEADER: \usepackage{tgcursor}

#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [tiny]
#+LATEX_HEADER: \usetheme[background=light]{metropolis}
#+LATEX_HEADER: \metroset{block=fill, baground=dark}
#+LATEX_HEADER: \setbeamertemplate{frame footer}{\color{lightgray}\includegraphics[height=0.5cm]{ferallogo.png}}
#+LATEX_HEADER: \usepackage[backend=bibtex]{biblatex}
#+latex_header: \BeforeBeginEnvironment{block}{\framebreak}
#+LaTeX_HEADER: \institute[INST]{Foundation for Ecological Research, Advocacy and Learning (FERAL). \\\url{https://www.feralindia.org}}
#+LaTeX_HEADER: \usepackage{url}
#+LaTeX_HEADER: \usepackage{minted}
#+LaTeX_HEADER: \usemintedstyle{emacs}
#+PANDOC_OPTIONS: toc: true
#+PANDOC_OPTIONS: toc_float: true
#+PANDOC_OPTIONS: toc-depth: 2
#+STARTUP: align fold nodlcheck hidestars oddeven lognotestate hideblocks indent content beamer
#+PROPERTY: session *R*
#+PROPERTY: cache yes
#+PROPERTY: results output verbatim graphics
#+PROPERTY: header-args :tangle yes :eval no-export
#+PROPERTY: header-args:R :session *R* :exports both

* Introduction

** Why use R for Remote Sensing?
- Reproducible and reusable code.
- Multi-threaded.
- Vast array of analytical tools.
- Visualisation.
- Libraries and functions, both from R and from other packages including Python, GRASS, SAGA etc.

** What you need
#+begin_src R :results none
libs <- c("sf", "maptools", "raster", "parallel", "ggplot2", "plyr", "reshape2", "scales", "gridExtra", "cowplot", "rcartocolor", "ggspatial", "rgdal", "gdalUtils")
lapply(libs, library, character.only = TRUE)
#+end_src

* Getting the data in

** Types of raster data:
You can import raster data as:
- Individual rasters.
- A stack of rasters is a collection of RasterLayer objects with the same spatial extent and resolution. A RasterStack can be created from RasterLayer objects, or from raster files, or both.
- A brick of rasters, RasterBrick is a multi-layer raster object. They are typically created from a multi-layer (band) file. They are similar to a RasterStack (that can be created with stack), but are less flexible as they can only point to a single file.

** Example of NDVI
#+begin_src R :results graphics file :file ./res/ndvi.png
red <- raster("./data/Mukurthi/b3.tif")
nir <- raster("./data/Mukurthi/b4.tif")
ndvi <- (nir - red) / (nir + red)
image(ndvi)
#+end_src
#+attr_latex: :width 0.45\textwidth
#+RESULTS:
[[file:./res/ndvi.png]]

** Create a function for NDVI
You can create a bunch of functions for indices that you use often. Here is an example of one for ndvi.
#+begin_src R :results none
indx.ndvi <- function(red, nir){
ndvi <- (nir - red) / (nir + red)
return(ndvi)
}
#+end_src

** Using the NDVI function
\tiny
Plot function from <https://stackoverflow.com/questions/9436947/legend-properties-when-legend-only-t-raster-package>
#+begin_src R :results graphics file :file ./res/func-ndvi.png
library(colorRamps )
r <- indx.ndvi(red = red, nir = nir)
plot(r, col = blue2green(100), legend=FALSE, axes=FALSE)
r.range <- c(round(minValue(r), 2), round(maxValue(r), 2))
plot(r, legend.only=TRUE, col=blue2green(100 ),
     legend.width=1, legend.shrink=0.75,
     axis.args=list(at=seq(r.range[1], r.range[2], 25),
                    labels=seq(r.range[1], r.range[2], 25),
                    cex.axis=0.6),
     legend.args=list(text='NDVI', side=4, font=2, line=2.5, cex=0.8))
#+end_src
#+attr_latex: :width 0.25\textwidth
#+RESULTS:
[[file:./res/func-ndvi.png]]

* A sample of my work in South Sudan
** A note on this data
- This is from my present work in South Sudan where I'm in the final stages of describing biophysical attributes of sites.
- Government colleagues are still finalising the actual micro-watersheds to select.
- I need to be able to complete this analysis as soon as they pick three of these micro-watersheds.
- Vector data used here is from public sites including the National Bureau of Statistics (RoSS), World Bank and OCHA.
- Climatic trends were extracted from USGS data on Google Earth Engine with many inputs from Pradeep Koulgi.
- The LULC data is from ESA and is both the rasters and a csv file that holds the classes.
- Other data sources have been shared with you earlier.

** The Republic of South Sudan with Waterbodies and PAs
[[./RoSS.png]]

** Map of the potential sites (micro-watersheds)
#+attr_latex: :height 1.0\textheight
[[./MicWS.png]]

** Import the Rasters
These have been clipped to the extent of the two counties which were pre-selected for the project.
Note: this is a subset of the data.
Also note: Lists are more efficient, the last line converts the list into its respective parts and can be dropped if you're comfortable with lists.
#+begin_src R :output none
rst.list <- list.files("./data/clim", full.names=TRUE, pattern=".tif$")
rst.names <- gsub(".tif", "", list.files("./data/clim", full.names=FALSE, pattern = ".tif$"))
lst <- lapply(rst.list, FUN = raster)
names(lst) <- rst.names
list2env(lst, envir=.GlobalEnv) # extract the elements of the list
#+end_src

#+RESULTS:

** Import the Vector
I've got three probable micro-watersheds, but I can always change these later if asked to.
#+begin_src R :results graphics file :file ./SelectedMicWS.png
selmws <- st_read(dsn = "./data/clim/RoSS.gpkg", layer = "SelWS")
plot(st_geometry(selmws), col = "red")
#+end_src
#+attr_latex: :width 0.45\textwidth
#+RESULTS:
[[file:./SelectedMicWS.png]]

** Define the objective
I need to figure out what the trends and the land use under my selected micro-watersheds is. I need the results in the form of a bar plot which is simple to understand by non-tech folks.
Therefore my script should be able to:
1. Clip the raster to the extent of the selected micro-watersheds.
2. Extract the values of the rasters as a histogram in the case of continuous data (trends) OR as categories for the LULC and similar categorical maps.
3. Visualise the results dump them as graphics I can import into my reports.

To do this I created two functions which I could ~apply~ onto my data. This is similar to what Pradeep did with his functions which he ~mapped~ the data to in the Earth Engine.

** Fn.1: Histogram of continuous raster within vector boundaries
#+ATTR_LATEX: :options fontsize=\ttfamily\tiny
#+begin_src R :results none
hist.continuous <- function(rst, vect, filename) {
  # transform vect to the rasters crs
  crs.to <- crs(rst, asText = TRUE)
  if(crs.to != crs(vect, asText = TRUE)) {
    vect <- st_transform(vect, crs.to )
  }
  rst <- crop(rst, extent(vect))
  rst <- mask(rst, vect)
  if(crs(rst, asText = TRUE) == "+proj=longlat +datum=WGS84 +no_defs") {
    rst <- projectRaster(rst, crs = 20135 )
  } # transform raster to utm
  f <- hist(rst, breaks=30, plot = FALSE)
  dat <- data.frame(counts= f$counts, breaks = f$mids)
  dat$area_ha <- prod(res(rst))*dat$counts/10000 # multiply frequency with pixel resolution
  colnames(dat ) <- c("Pixels", "Value",  "AreaHa")
  plt <- ggplot(dat, aes(x = Value, y = AreaHa, fill = Value)) +
    geom_bar(stat = "identity", alpha = 0.8)+
    xlab("xlab")+
    ylab("Area Ha")+
    guides(fill=guide_legend(title="xlab"))+
    scale_x_continuous() +
    scale_fill_gradient(low="red", high="green")
## plt
  ggsave(plot = plt,  filename = filename, device = "png", width = 12, height = 8, units = "in", dpi = 300)
 }
#+end_src

** Lets run it on the Palmer Drought Severity Index
#+begin_src R :results none
hist.continuous(rst = pdsiTrendsAweil_Max.Annual, vect = selmws, filename = "./res/pdsiHist.png")
#+end_src
#+attr_latex: :width 0.65\textwidth
[[./res/pdsiHist.png]]

** Fn. 2: Histogram of categorical raster within vector boundaries
#+ATTR_LATEX: :options fontsize=\ttfamily\tiny
#+begin_src R :results none
hist.categorised <- function(rst, vect, class, filename) {
  # transform vect to the rasters crs


  crs.to <- crs(rst, asText = TRUE)
  if(crs.to != crs(vect, asText = TRUE)) {
    vect <- st_transform(vect, crs.to )
  }
  rst <- crop(rst, extent(vect))
  rst <- mask(rst, vect)
  if(crs(rst, asText = TRUE) == "+proj=longlat +datum=WGS84 +no_defs") {
    rst <- projectRaster(rst, crs = 20135 )
  } # transform raster to utm
  rst.df <- as.data.frame(rst)
  names(rst.df) <- "Category"
  colnames(vegclass)[c(1, 2)] <- c("Category", "Cover")
  rst.df <- merge(rst.df, vegclass, by = "Category")
  rst.df <- count(rst.df, "Cover" )
  rst.df$AreaHa <- prod(res(rst))*rst.df$freq / 10000 # multiply frequency with pixel resolution
  plt <- ggplot(rst.df, aes(x=Cover, y=AreaHa, fill = Cover)) +
    geom_bar(stat = "identity") +
    scale_fill_brewer(palette = "Set1") +
    labs(y = "Area in Ha")+
    scale_x_discrete(guide = guide_axis(n.dodge = 2))+
    theme(
      legend.position="none",
      axis.text.x = element_text( vjust = 0.5, hjust= 0.5))
  # plt
  ggsave(plot = plt,  filename = filename, device = "png", width = 12, height = 8, units = "in", dpi = 300)
}
#+end_src

** Run in on the European Space Agency LULC map of Africa
Read up about the dataset [[http://2016africalandcover20m.esrin.esa.int/][here]].
#+begin_src R :results none
lulc.class <- read.csv("./data/clim/lulc.csv", sep=";")
hist.categorised(rst = lulc, vect = selmws, class = lulc.class, filename = "./res/lulcHist.png")
#+end_src
#+attr_latex: :width 0.65\textwidth
[[./res/lulcHist.png]]

* Optional Exercises
** Consider This
- A list of functions for the indices you use most often (perhaps along with plotting).
- A ggplot2 function to such as the ones above to help describe your raster data.

** NBR
#+begin_src R
indx.nbr <- function(nir, swir, filename){
  nbr <- (nir - swir) / (nir + swir)
  nbr.df <- as.data.frame(nbr)
  # TBD: create categorical map
  f <- hist(nbr, breaks = 30, plot = FALSE)
  dat <- data.frame(counts= f$counts, breaks = f$mids)
  dat$area_ha <- prod(res(nbr))*dat$counts/10000
  colnames(dat ) <- c("Pixels", "Value", "AreaHa")
  plt <- ggplot(dat, aes(x = Value, y = AreaHa, fill = Value)) +
    geom_bar(stat = "identity", alpha = 0.8)+
    xlab("xlab")+
    ylab("Area Ha")+
    guides(fill=guide_legend(title="xlab"))+
    scale_x_continuous() +
    scale_fill_gradient(low="red", high="green")
  ## plt
  ggsave(plot = plt, filename = filename, device = "png", width = 12, height = 8, units = "in", dpi = 300)
}
#+end_src

#+RESULTS:

#+begin_src R
nir <- raster("./data/Mukurthi/b4.tif")
swir <- raster("./data/Mukurthi/b7.tif")
filename <- "./res/nbrHist.png"
indx.nbr(nir, swir, filename)
#+end_src

#+RESULTS:

[[./res/nbrHist.png]]
* Resources

** Raster analysis

- [[https://cran.r-project.org/web/packages/raster/raster.pdf][Raster package documentation]].
