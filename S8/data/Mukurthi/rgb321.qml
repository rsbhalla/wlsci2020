<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="1.8.0-Lisboa" minimumScale="0" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <transparencyLevelInt>255</transparencyLevelInt>
  <rasterproperties>
    <mDrawingStyle>SingleBandPseudoColor</mDrawingStyle>
    <mColorShadingAlgorithm>PseudoColorShader</mColorShadingAlgorithm>
    <mInvertColor boolean="false"/>
    <mRedBandName>Not Set</mRedBandName>
    <mGreenBandName>Not Set</mGreenBandName>
    <mBlueBandName>Not Set</mBlueBandName>
    <mGrayBandName>Band 1</mGrayBandName>
    <mStandardDeviations>0</mStandardDeviations>
    <mUserDefinedRGBMinimumMaximum boolean="false"/>
    <mRGBMinimumMaximumEstimated boolean="true"/>
    <mUserDefinedGrayMinimumMaximum boolean="false"/>
    <mGrayMinimumMaximumEstimated boolean="true"/>
    <mContrastEnhancementAlgorithm>NoEnhancement</mContrastEnhancementAlgorithm>
    <contrastEnhancementMinMaxValues>
      <minMaxEntry>
        <min>0</min>
        <max>255</max>
      </minMaxEntry>
    </contrastEnhancementMinMaxValues>
    <mNoDataValue mValidNoDataValue="true">0.000000</mNoDataValue>
    <singleValuePixelList>
      <pixelListEntry pixelValue="0.000000" percentTransparent="100"/>
    </singleValuePixelList>
    <threeValuePixelList>
      <pixelListEntry red="0.000000" blue="0.000000" green="0.000000" percentTransparent="100"/>
    </threeValuePixelList>
  </rasterproperties>
</qgis>
