#+title: Introduction to GIS and Remote Sensing
#+subtitle: Vector GIS Session 3 - Digitising
#+author: R.S. Bhalla and Tara Rajendran
#+email: rsbhalla@gmail.com; bhalla@feralindia.org
#+tags: @Ravi(r) @Tara(t) @Students(s)
#+LATEX_HEADER: \RequirePackage{fancyvrb}
#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{fontsize=\tiny,formatcom = {\color[rgb]{0.5,0,0}}}
#+PANDOC_OPTIONS: toc: true
#+PANDOC_OPTIONS: toc_float: true
#+PANDOC_OPTIONS: toc-depth: 2
#+PROPERTY: session *R*
#+PROPERTY: cache yes
#+PROPERTY: results output verbatim graphics
#+PROPERTY: header-args :tangle yes :eval no-export
#+PROPERTY: header-args:R :session *R* :exports code
#+STARTUP: align fold nodlcheck hidestars oddeven lognotestate hideblocks indent content beamer

* Digitising - Vector GIS Session 3

** Scale
A scale of a map is the ratio of the distance on the map to that on the on-ground. A small scale map is that with a smaller ratio, such as one showing a country or state as opposed to large scale map such as the cadastral map which is about 1:5,000. Scale in GIS is important because it alters the perception of the map and the amount of information that can be shown on it. Typically a large scale map shows a lot more detail (is close up). For example if  you zoom into an online OSM map, labels of streets and even individuals shops may pop-up, but these disappear as the scale decreases, i.e. as you zoom out.

The scale of a map determines what it can be used for. Inversely, the type of data and analysis used to develop a map also determines at what scale it can be produced.

The scale of a map also becomes important while dealing with images. There is no point in showing finer resolution data (individual trees in a forest) if the map is only going to be viewed at a forest or district resolution. This is the reason why on-line raster datasets often use an image pyramid to show you low resolution images while at a low scale. A raster pyramid is a sequence of overviews of the original image, designed to be seen at different scales.

QGIS allows you to set scale based visibility for your vector data labels and styles. You can also set QGIS to change the way it renders layers at different scales and to switch export options so that complex geometries are exported as raster images at certain scales.

*** TODO Exercise                                         :@Tara:@Students:
What are the typical scales at which these maps are available?
| Sl | Type of map                                                            | Common scales |
|----+------------------------------------------------------------------------+---------------|
| 1. | SoI toposheets                                                         |               |
| 2. | Geological SoI Maps                                                    |               |
| 3. | Soil maps from NBSSLUP                                                 |               |
| 4. | Revenue or cadastral maps from various dpt. of Survey and Land Records |               |
| 5. | Forest SoI maps                                                        |               |

*** TODO Exercise                                               :@Students:
Set the building layer you digitised so that the labels of the type of building is only ~rendered~ at a 1:2000 or higher scale.

** Multiple and single geometries
There is a 'multi' version of most of the vector geometries you can digitise. These are typically those geometries that share a single attribute. For example, if your attributes are the details of persons (name address etc.), all the fields of a map owned by that person may point to just that attribute. Roads are another feature which often involves the use of multiple geometries, lines in this case. QGIS allows you to go back and forth between single and multiple geometries.

There are other more complex geometries that you need to be familiar with. Islands and holes, for example. The former, as the name suggests is a polygon embedded in, or surrounded by a different polygon but one which shares the same boundaries. A hole, as you probably guessed, is its opposite, i.e. a polygon in which there is a 'hole'. A farmer field with a pond would be an example of this situation. If you're calculating yield on the basis of area cropped, you would probably want to exclude the area under the pond.

** Snapping and topological editing
It is impossible to digitising common boundaries without ensuring there is an overlap or gap. QGIS fixes this by providing you two kinds of tools.
1. Setting the snapping options while digitising to snap to vertex and segment.
2. Enabling topographic editing and setting it to remove overlaps.

This is demonstrated in the video on topological editing.

*** TODO Exercise                                               :@Students:
Digitise four adjacent boundaries on the cadastral map ~cadmap_utm.tif~ using topological editing. Insert an island and a hole in these polygons using the appropriate tool.

** Attributes
This refers to the non-spatial data connected to the geometries of a vector, for example, the name of the farmer owning a particular plot or the depth of a well or the soil type at a particular coordinate. Attributes work together with their location to make GIS a powerful tool. One has to be careful while designing and populating attribute tables. Key issues to keep in mind are:

- Standardisation :: Labels need to match if they are for the same object. For e.g. Paddy, paddy, rice, Rice are four different labels for what is probably the same crop. Standardisation therefore needs to be across spellings and the use of lower and upper case characters.
- Coding :: This is similar to standardisation. While dealing with descriptive datasets, its better to have a standard code as a single word where possible. For example, for the question: Why don't you collect fuelwood from XYZ forest area? The responses might be coded as:
  - distance :: It is too far away
  - security :: It is too dangerous to go there
  - protection :: It is not allowed by the department
  - unprofitable :: It is not financially worth while
- Scaling :: It is usually to scale responses. E.g. a three point scale (1, 2, 3) for poor, OK, good.
- Using numeric data where possible :: Numbers are easier to work with for statistical analysis.

QGIS has a very easy to use form designer to help you enter attribute data on layers (Layers -> Properties -> Attributes Form). The designer can be provided a number of constraints which ensure data entered is standardised and ready for further analysis. QGIS also has some nice tools to help with basic analysis of attributes. Here are some of the ones you should be aware of:

1. Group Stats - the equivalent of a pivot table, for QGIS.
2. Point Sampling Tool - Allows for collection of attributes from multiple layers, both raster and vector.
3. MMQGIS - CSV joins and a bunch of other CSV and attribute management tools.
4. Extract, select and join by attribute - all in the processing toolbar.

** Topology
There are relationships of a geometric object with itself and other geometries. For example does a line ~intersect~ with itself or with another line. Typology rules can specific to different types of geometries. There are also rules that can be set by the user which constrain geometries to specific parameters. For example, a polygon is required to be ~closed~. Polygons on adjacent plots may not be permitted to have ~gaps~ or ~overlaps~ between their borders. Similarly a line may not be allowed to ~cross~ itself or a polygon may not be permitted to twist around itself. These sets of rules are what is called a topological data model

Topological formats such as geo-package, allow the user to check for incorrect geometries using these rules/constraints. This is particularly useful while checking a large dataset such as all the roads in a city or survey plots in a village. For example, the [[https://docs.qgis.org/3.10/en/docs/user_manual/plugins/core_plugins/plugins_topology_checker.html][Topology Checker plugin]] allows you to test for the following errors in polygons:
- Polygon rings must close.
- Rings that define holes should be inside rings that define exterior boundaries.
- Rings may not self-intersect (they may neither touch nor cross one another).
- Rings may not touch other rings, except at a point.
