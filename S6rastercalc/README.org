#+title: Introduction to GIS and Remote Sensing
#+subtitle: Remote Sensing Session 6
#+author: R.S. Bhalla and Tara Rajendran
#+email: rsbhalla@gmail.com; bhalla@feralindia.org
#+tags: @Ravi(r) @Tara(t) @Students(s)
#+LATEX_HEADER: \RequirePackage{fancyvrb}
#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{fontsize=\tiny,formatcom = {\color[rgb]{0.5,0,0}}}
#+PANDOC_OPTIONS: toc: true
#+PANDOC_OPTIONS: toc_float: true
#+PANDOC_OPTIONS: toc-depth: 2
#+PROPERTY: session *R*
#+PROPERTY: cache yes
#+PROPERTY: results output verbatim graphics
#+PROPERTY: header-args :tangle yes :eval no-export
#+PROPERTY: header-args:R :session *R* :exports code
#+STARTUP: align fold nodlcheck hidestars oddeven lognotestate hideblocks indent content beamer

* Summary
1. A [[file:Intro_Datasensors_Jagdish.pdf][presentation]] (by Jagdish Krishnaswamy - ATREE) on what data sensors are with some examples of what remote sensing (RS) is used for. There will be some presentation on this by guest speakers in the coming days.
2. Exercises on the raster calculator which introduce you to the world of image indices. There will be individual assignments on this.
3. A [[file:videos/S6RasterCalc.mp4][short video]] on using the raster calculator to calculate NDVI.

* Things you should remember

1. The resolution of a raster is defined by a number of properties:
   1. Spatial: the pixel size.
   2. Radiometric: the number of levels of brightness each pixel holds.
   3. Spectral: the number of bands of the image, or which portions of the spectrum its bands cover.
   4. Temporal: the return period, time (days) for it to take a scene (shot) of the same part of the earth.
   5. Swath width or extent: the area of the earth the image covers.
2. Sensors are designed for specific applications, most of them differ in one or more properties. This means it is usually not possible to compare an image of the same area acquired by different sensors. Some of the properties, such as spatial resolution, can be adjusted statistically, but others such as spectral range, cannot.
3. It is a good practice to read the metadata about an image (the information tab in the properties dialog of QGIS) and its histogram. The histogram describes the frequency at which different pixels occur.
4. Digital elevation models are among the most useful remotely sensed data available and are used to derive a number of features such as contour lines, hydrologic delineations and stream channels. DEMs data is used extensively to explain ecological processes.
5. There are a number of satellite derived indices and models which have become commonplace in ecological research. NDVI is one of them, however there are many more. Look at the resources provided below for some examples.
6. Assigning different bands of an image to the red, green and blue display of a monitor is called image composting. These false colour combinations, coupled with changing of display properties such as saturation and contrast, can be a useful way to identify specific on-ground features. They can also be used as input bands during image classification.
7. There are also a number of satellite products made available for direct application in climate change analysis for instance. See the resources section for more details.
8. Even though there are a number of 'clean' products available today, you should know about the types of distortions and errors that creep into a remotely sensed product - from errors in the sensors, to atmospheric effects.

* Class exercise/assignments

** What affects physiognomy?
You need to answer three questions:

1. Does forest composition or physiognomy vary by +elevation or other terrain characteristics+  (aspect)?

2. Is there a relationship between vegetation index and forest type or physiognomy?

3. Test whether forest type/physiognomy can be classified using elevation/terrain AND vegetation index?

*** Tools you should know how to use

1. Raster calculator
   1. Conditional statements,
   2. Extract vegetation indices
2. Terrain analysis
3. Raster to vector and vector to raster conversions
4. Randomisation within and without sub-sets
5. Point sampling tool

*** Data needed for the exercises
1. A digital elevation model
2. A physiognomy/floristic layer (converted from a vector to a raster)
3. A vegetation index layer: EVI= G*(NIR-E)/(NIR+C1*R-C2*B+L)
   C1: 6
   C2: 7.5
   L: 1
   G: 2.5
   ========
   SAVI = (1+L)(NIR-R)/(NIR+R+L)
   L: 0.5 (0 to 1). 0 in dense and 1 in no vegetation.
*** Methods
**** For q. 1 and 2
1. Use the randomisation tool and point sampling tool
2. Sample the elevation/terrain, vegetation index values and the physiognomy/floristic description for the layers.
3. See if you can spot a pattern.
You're encouraged to use R for this.

**** For q3
First select two elevation/terrain based and vegetation index value range pairs:

 | Physiog | SlopeMin | SlopeMax | VegMin | VegMax |
 |---------+----------+----------+--------+--------|
 |         |          |          |        |        |
 |         |          |          |        |        |

***** Data req:
1. [X] Physiognomy
2. [X] Slope
3. [X] VegIndex

2. Convert the two layers to a single vector where the attribute table shows which category they belong to.
   1. Use the randomisation tool to generate a random sample between classes
   2. Use the point sampling tool to extact the data for further analysis.
   You're encouraged to use R for this.

** What affects diversity and productivity more, elevation, aspect or rainfall?
You need to organise the data for this and explain what analysis you'll do. You're not expected to do the actual analysis, although it would be a good exercise.
Please note you've been provided Landsat7 data in the sample files. You'll need to download Landsat8 data for the assignment. Also note, not all the PAs have all the data required to answer the question, please select the PA carefully.
Finally, no two persons are allowed to pick the same PA. I've already picked Mukurthy, so that is ruled out as well.

The question is:
Which of the following has the biggest impact on:
1. Floristic diversity +(join the ~veg_ft~ and the ~Florist_Types_FT_Codes~)+
2. Productivity (any vegetation index of your choice)
3. Major forest types (join the ~veg_mt~ and the ~MajorForestTypes_MFT_Codes~)
The independent variables are:
1. Elevation
2. Aspect
3. Rainfall
You MUST generate random points and use the point sampling tool to harvest the data for your analysis. Remember we are interested in the data and the process used to extract it, not the answer.

** Using the Raster Calculator for conditional statements

[[file:videos/S6RasterCalc-2.mp4][The video]] shows you these four types of calculations on the Raster Calculator:

- NDVI :: ("b4@1" - "b3@1")/("b4@1" + "b3@1")
- MASK :: ("b4@1" - "b3@1")/("b4@1" + "b3@1") > 0.5 AND "aster@1"> 800
- MASK * NDVI :: ((("b4@1" - "b3@1")/("b4@1" + "b3@1") > 0.5) AND "aster@1"> 800) * ("b4@1" - "b3@1")/("b4@1" + "b3@1")
- Classified NDVI :: (("NDVI@1" >-1) AND ("NDVI@1" < 0)) * 1 +
  (("NDVI@1" > 0) AND ("NDVI@1" < 0.2)) * 2 +
  (("NDVI@1" > 0.2) AND ("NDVI@1" < 0.35)) * 3 +
  (("NDVI@1" > 0.35) AND ("NDVI@1" < 0.45)) * 4 +
  (("NDVI@1" > 0.45) AND ("NDVI@1" < 1)) * 5

** Criteria analysis exercise

- Identify the core zones of your PA :: You have a PA and layers that can be used to assign a "conservation value" to parts of the PA. You can also derive layers using band combinations and/or image indices which highlight specific areas of interest.

When you have four to five layers - assign weights to each of them and then add them up to develop a map showing the core zones.

Elements that you should consider using:
1. Vector to raster (IFP layers under the PA dataset)
2. Band combination to visually identify areas of interest. You can use the raster calculator to extract ranges of the RGB bands to create classes. You then can assign weights to the classes.
3. Image indices. Create classes using the raster calculator and then assign weights.
4. Combine these layers using any algebraic procedure you see fit.

Each group comes up with a problem. Each INDIVIDUAL solves and submits it during class. Then the groups rotate their problem but again, these are to be worked on as individuals.

We will discuss how different persons tackled this challenge during class.

* Resources
- [[https://lpdaac.usgs.gov/tools/][Earth Data on USGS]] :: Bunch of tools and sites to access data from USGS.
- [[https://wiki.landscapetoolbox.org/][The landscape toolbox]] :: Fantastic resource for image indices and their applications.
- [[https://www.usgs.gov/core-science-systems/nli/landsat][Landsat Missions on USGS]] :: Must read on the Landsat story.
- [[https://sentinel.esa.int/web/sentinel/home][Sentinel Online]] :: About the ESA Sentinel missions
  - [[https://spacedata.copernicus.eu/][Copernicus Data Access]] :: Just that
  - [[http://www.esa.int/Applications/Observing_the_Earth/Copernicus][Copernicus missions]] :: ESA Copernicus page
- [[https://earthengine.google.com/][Google Earth Engine]] :: Google server farms at your disposal for remote sensing and analysis.
- [[https://www.opendatacube.org/][Open Data Cube]] :: Open Source Geospatial Data Management & Analysis Platform supports OGC Web Service.
- Sources of data :: Some datasets that I use.
  1. NEX-GDDP
     The NASA NEX-GDDP dataset is comprised of downscaled climate scenarios for the globe from 1950-01-01 to 2099-12-31. This dataset was prepared by the Climate Analytics Group and NASA Ames Research Center using the NASA Earth Exchange, and distributed by the NASA Center for Climate Simulation (NCCS).
  2. FLDAS
     The Famine Early Warning Systems Network (FEWS NET) Land Data Assimilation System consists of a number of parameters collected from 1982 which are used for drought monitoring and famine early warnings. The FLDAS uses data from both Modern-Era Retrospective analysis for Research and Applications version 2 (MERRA-2) and Climate Hazards Group InfraRed Precipitation with Station (CHIRPS) for the trend analysis.
  3. TerraClimate
     TerraClimate is a high-resolution global dataset of monthly climate and climatic water balance from 1958 till the present. The products includes essential climate variables, evapotranspiration estimates and water balance variables. The data set is created by combining high-spatial resolution climatological normals from the WorldClim dataset, with coarser spatial resolution, but time-varying data from [[http://data.ceda.ac.uk//badc/cru/data/cru_ts/][CRU Ts4.0]] and the [[http://jra.kishou.go.jp/JRA-55/index_en.html][Japanese 55-year Reanalysis]].
  4. Terra Vegetation Indices
     This data source provides the Normalized Difference Vegetation Index (NDVI) and the Enhanced Vegetation Index (EVI) from 18th February 2000 to the present. The data is a 16 day global mosaic at 250m. Both the indices provide a measurement of plant greenness or health. While the NDVI is used more widely, the EVI is considered a superior alternative as it minimizes canopy background variations and maintains sensitivity over dense vegetation conditions where NDVI tends to saturate.
  5. MODIS Terra Gross Primary Productivity
     The MODIS Terra MOD17A2H.006: 8-Day Global 500m image from [[https://doi.org/10.5067/MODIS/MOD17A2H.006][NASA LP DAAC at the USGS EROS Center]] provides the gross primary production and net photosynthesis i.e. GPP minus the maintenance respiration (MR). This product is available from February 18^{th }2000 to the present.
  6. Terra Net Primary Production
     Net primary productivity is the difference between the rate at which plants in an ecosystem produce useful chemical energy or Gross Primary Productivity (GPP), and the rate at which they expend some of that energy for respiration. This gap filled yearly global data set at 500m from the year 2000 to the present contains a single band (and quality control percentages).
  7. CHIRPS Daily Rainfall
     This is the Climate Hazards Group InfraRed Precipitation with Station Data (CHIRPS) provides data from 1981 till the present in 0.05 arc degree tiles. CHIRPS is among the most reliable remotely sensed rainfall datasets and is used extensively for modelling drought and vulnerability in data deficient regions.
